A memória RAM de um computador é uma sequência de bytes. A posição 1,2,
..,n que um byte ocupa na memória é o endereço do byte.
Cada variável de um programa ocupa um certo número de bytes consecutivos
na memória.
Uma variável char ocupa 1 byte, um int 4 bytes, um float 4 bytes e um
double ocupa 8 bytes. O número exato de quantos bytes cada tipo ocupa
é dado pela operação sizeof(), que devolverá o número exato em bytes.

O endereço de uma variável i é &i.

Uma variável do tipo ponteiro, é uma variável que armazena endereços, se
um ponteiro p tem valor diferente de NULL então *p. Se um ponteiro p
armazena o endereço de uma variável i, podemos dizer "p aponta para i"
ou "p é o endereço de i".

i uma variável e p vale &i então dizer "*p" é o mesmo que dizer "i".
& = endereço
* = Valor do endereço apontado

Ponteiros são fortemente tipados, logo temos int *p, float *p etc.

Para imprimir um endereço usa-se &p, printf("%p\n", &i);
