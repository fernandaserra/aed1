void inserir(tipoLista *L, int *d)
{
  tipoNo *aux = (tipoNo *) malloc(sizeof(tipoNo));
  if (!aux) exit(1);
    aux->dado = *d;
    aux->prox = L->prim;
    L->prim = aux;
}
/*
  A primeira alteração feita é a passagem de l(tipoLista) por referência,
já que a lita será alterada, de outro modo as alterações feitas na lista não persistirão.
	A segunda alteração é a alocação de memória para o tipo aux, pois desse modo pode-se criar um
novo elemento, caso não fosse alocado esse elemento seria perdido.
  Essa nova alocação de memória para aux, deve ser testada antes de usada, logo verifica-se ela
não está apontando para NULL.
	A Terceira e última alteração é que na linha 5, aux->dado recebe a variável apontado por d
e não d.
	   */
