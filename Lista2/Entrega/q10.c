#include<string.h>
void quick(tipoDados v[], int inicio, int fim)
{
  tipoDados pivot, aux; int i, j;
  if((fim - inicio) > 10)
  {
    pivot = v[(inicio + fim)/2];
    i = inicio; j = fim;
    do
    {
      for(;strcmp(v[i].nome, pivot.nome) < 0; i++);
      for(;strcmp(v[j].nome, pivot.nome) > 0; j--);
      if(i <= j)
      {
        aux = v[i];
        v[i] = v[j]; v[j] = aux;
        i++; j--;
      }
    }while (i <= j);
    quick(v, inicio, j);
    quick(v, i, fim);
  }else
  {
    for(i = 1; i < (fim + 1); i++)
    {
      pivot = v[i];
      j = i - 1;
      if(strcmp(v[j].nome, pivot.nome) > 0)
      {
        for(;(j >= 0) && (strcmp(v[j].nome, pivot.nome) > 0); j--) v[j + 1] = v[j];
        v[j + 1] = pivot;
      }
    }
  }
}

void quickSort(tipoDados v[], int n)
{
  quick(v, 0, (n - 1));
}
