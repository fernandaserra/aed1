#include<stdio.h>
#include<stdlib.h>
typedef struct tipoNo
{
  int dado;
  struct tipoNo *prox;
}tipoNo;

typedef struct tipoLista
{
    tipoNo *prim;
}tipoLista;

void transfDados(tipoLista *l, int n, int v[n])
{
  tipoNo *aux = (tipoNo *)malloc(sizeof(tipoNo));
  if(!aux) exit(1);
  aux->dado = v[0];
  aux->prox = NULL;
  l->prim = aux;
  for (int i = 1; i < n; i++)
  {
    aux = (tipoNo *)malloc(sizeof(tipoNo));
    aux->dado = v[i];
    aux->prox = l->prim; l->prim = aux;
  }
}
