int dividir(tipoDados v[], int inicio, int fim)
{
  tipoDados aux, pivot = v[fim];
  int i = (inicio - 1);
  for (int j = inicio; j <= (fim - 1); j++)
  {
    if(v[j].id <= pivot.id)
    {
      i++;
      aux = v[i];
      v[i] = v[j]; v[j] = aux;
    }
  }
  aux = v[i + 1];
  v[i + 1] = v[fim]; v[fim] = aux;
  return (i + 1);
}

void quickSort(tipoDados v[], int inicio, int fim, int n)
{
  if(inicio < fim)
  {
    int size = dividir(v, inicio, fim);
    quickSort(v, inicio, (size - 1), n);
    if(size < (n - 1)) quickSort(v, (size + 1), fim, n);
  }
}

void quick(tipoDados v[], int n, int x)
{
  quickSort(v, 0, (n - 1), x);
}
