int soma(tipoLista l)
{
  int cont = 0;
  while (l.prim)
  {
    cont += l.prim->dado;
    l.prim = l.prim->prox;
  }
  return cont;
}