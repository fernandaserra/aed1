int simetrica(tipoLista l)
{
  tipoNo  *inicio = l.prim , *meio = l.prim;
  int s = 0, s1 = 0, m = 0, m1 = 0;
  for(int i = 0; (l.prim->prox); i++) l.prim = l.prim->prox;
  i = i/2;
  for(int j = 0; j <= i; j++) meio = meio->prox;
  l.prim = inicio;

  while (meio)
  {
    s += meio->dado; m *= meio ->dado;
    s1 += l.prim->dado; m1 *= l.prim->dado;
    meio = meio->prox;
    l.prim = l.prim->prox;
  }
  if((s == s1) && (m == m1)) return 1;
  else return 0;
}
