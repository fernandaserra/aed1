#include<string.h>
void merge(tipoDados v[], tipoDados v1[], int inicio, int fim)
{
  int meio = (inicio + fim)/2, i, j, x;
  if(inicio < fim)
  {
    merge(v, v1, inicio, meio);
    merge(v, v1, (meio + 1), fim);
    x = inicio; i = inicio; j = meio + 1;
    while ((i <= meio) && (j <= fim))
    {
      if(strcmp(v[i].nome, v[j].nome) < 0)
      {
        v1[x] = v[i]; i++;
      }else
      {
        v1[x] = v[j]; j++;
      }
      x++;
    }
    while (i <= meio)
    {
      v1[x] = v[i];
      i++; x++;
    }
    while (j <= fim)
    {
      v1[x] = v[j];
      j++; x++;
    }
    for(int x = inicio; x <= fim; x++) v[x] = v1[x];
  }
}

void mergeSort(tipoDados v[], int n)
{
  tipoDados *v1 = (tipoDados *)malloc(sizeof(tipoDados)*n);
  merge(v, v1, 0 , (n - 1));
}
