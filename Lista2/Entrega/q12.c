void quick(tipoDados v[], int inicio, int fim)
{
  tipoDados pivot, aux; int i, j;
  if(inicio < fim)
  {
    pivot = v[(inicio + fim)/2];
    i = inicio; j = fim;
    do
    {
      for(; v[i].id < pivot.id; i++);
      for(; v[i].id > pivot.id; j--);
      if(i <= j)
      {
        aux = v[i];
        v[i] = v[j]; v[j] = aux;
        i++; j--;
      }
    } while(i <= j);
    quick(v, inicio, j);
    quick(v, i, fim);
  }
}

void quickSort(tipoDados v[], int n)
{
  quick(v, 0, (n -1));
}
