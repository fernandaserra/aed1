int contaChave(tipoLista l, int chave)
{
  tipoNo *aux; int cont = 0;
  if(!(l.prim)) return 0;
  aux = l.prim;
    do
    {
      if(l.prim->dado.id == chave) cont++;
      l.prim = l.prim->prox;
    } while(l.prim != aux);
  return cont;
}
