#include<stdio.h>
#include<stdlib.h>
typedef struct
{
	int id;
	char nome[20];
	char endereco[40];
}tipoDados;

typedef struct tipoNo
{
  tipoDados dado;
  struct tipoNo *prox;
}tipoNo;

typedef struct tipoLista
{
    tipoNo *prim;
}tipoLista;

void criar(tipoLista *l)
{
  l->prim = NULL;
}

void atualizar(tipoLista *l)
{
  l->prim = l->prim->prox;
}

void inserir(tipoLista *l, tipoDados x)
{
  tipoNo *aux = (tipoNo *)malloc(sizeof(tipoNo));
  if(!aux) exit(1);
  aux->dado = x;
  if(!(l->prim))
  {
    l->prim = aux;
    aux->prox = aux;
  }else
  {
    aux->prox = l->prim->prox;
    l->prim->prox = aux;
    l->prim = aux;
  }
}

void remover(tipoLista *l, int chave)
{
  tipoNo *aux, *aux2;
  tipoDados *x = (tipoDados *)malloc(sizeof(tipoDados));
  if(l->prim)
  {
    aux2 = l->prim;
    while ((l->prim->prox) != aux2)
    {
      aux = l->prim->prox;
      l->prim->prox = aux->prox;
      *x = aux->dado; free(aux);
      l->prim = aux2;
    }
    l->prim = l->prim->prox;
  }
  if((aux2->dado.id) == chave)
  {
    l->prim->prox = aux2->prox; l->prim = aux2->prox;
    *x = aux2->dado; free(aux2);
  }
}
