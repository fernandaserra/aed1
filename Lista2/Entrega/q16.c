void destruir(tipoLista *l)
{
  tipoNo *aux;
  while(l->prim)
  {
    aux = l->prim;
    l->prim = l->prim->prox;
    l->prim = NULL; free(aux);
  }
}
