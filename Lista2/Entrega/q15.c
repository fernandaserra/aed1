int crescente(tipoLista l)
{
  if(l.prim)
  {
    while(l.prim->prox)
    {
      if(l.prim->dado >= l.prim->prox->dado) return 0;
      l.prim = l.prim->prox;
    }
  }
  return 1;
}
