void remover(tipoLista *l, int n)
{
  int i = 0;
  tipoNo *aux = l->prim, *aux2;
  if(aux)
  {
    if(n == 1)
    {
      l->prim = aux->prox; free(aux);
    }else
    {
      aux2 = aux; aux = aux->prox;
        while(aux)
        {
          i++;
          if(i == n)
          {
            aux2->prox = aux->prox; free(aux);
            return;
          }
        }
        aux2 = aux; aux = aux->prox;
    }
  }
}
