#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct
{
  int id;
  char nome[20];
  char endereco[40];
}tipoDados;

typedef struct tipoNo
{
  tipoDados dado;
  struct tipoNo *prox;
}tipoNo;

typedef struct tipoLista
{
    tipoNo *prim;
}tipoLista;

void criar(tipoLista *l)
{
  l->prim = NULL;
}

void inserir(tipoLista *l, tipoDados x)
{
  tipoNo *aux = (tipoNo *)malloc(sizeof(tipoNo));
  if(!aux) exit(1);
  aux->dado = x;
  aux->prox = l->prim;
  l->prim = aux;
}

void remover(tipoLista *l)
{
  tipoNo *aux;
  if(l)
  {
    aux = l->prim;
    l->prim = l->prim->prox;
    free(aux);
  }
}
