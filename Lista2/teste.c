#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct
{
  char nome[256];
  char endereco[256];
  int id;
}tipoDados;

typedef struct tipoNo
{
  tipoDados d;
  struct tipoNo *prox;
}tipoNo;

void criar(tipoNo **l) //Para que a lista seja criada deve-se receber
{                     //um ponteiro duplo, que aponta para um endereço
  *l = NULL;          //de ponteiro, no caso o primeiro item da lista
}                     //que vai ser nulo,, pois não há nenhuma informação.

void inserir(tipoNo **l, tipoDados x)
{
  tipoNo *aux = (tipoNo *)malloc(sizeof(tipoNo)); //aloca o espaço de memória necessário p um novo nó
  if(!aux) exit(1);  //caso o aux continue vazio apos a alocação deu algo errado
  aux->d = x; //os dados são passados p novo nó
  aux->prox = *l;
  *l = aux;
}

void mostrar(tipoNo *l)
{
  while(l)
  {
    printf("ID:%d\nNome:%s\nEndereço:%s\n", l->d.id, l->d.nome,l->d.endereco);
    l = l->prox;
  }
}

void remover(tipoNo *l, int chave)
{
  tipoNo *aux;
    if(l)
    {
      aux = l;
      if(aux->d.id == chave)
      {
        l = l->prox->prox;
        free(aux);
      }
    }
}

int main()
{
  tipoNo *l1; /*tipoNo aqui é criado como um ponteiro pois ele vai apontar para
              o endereço de um ponteiro, o endereço do primeiro item da lista*/
  tipoDados d;
  criar(&l1);
  d.id = 0;
  strcpy(d.nome, "fer");
  strcpy(d.endereco, "vila humaita");
  inserir(&l1,d);
  d.id = 1;
  strcpy(d.nome, "fudida");
  strcpy(d.endereco, "vila do sofrimento");
  inserir(&l1,d);
  d.id = 2;
  strcpy(d.nome, "claudia");
  strcpy(d.endereco, "pipipi");
  inserir(&l1,d);
  d.id = 3;
  strcpy(d.nome, "bruno");
  strcpy(d.endereco, "rj");
  inserir(&l1,d);
  mostrar(l1);
  printf("****POS REMOÇÃO******\n");
  remover(l1,0);
  mostrar(l1);
  return 0;
}
