#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct
{
  int id;
  char nome[20];
  char endereco[40];
}tipoDados;

typedef struct tipoNo
{
  tipoDados dado;
  struct tipoNo *prox;
}tipoNo;

typedef struct tipoLista
{
    tipoNo *prim;
}tipoLista;

void criar(tipoLista *l)
{
  l->prim = NULL;
}


void mostrar(tipoLista l)
{
  while (l.prim)
  {
    printf("%d\n", l.prim->dado.id);
    l.prim = l.prim->prox;
  }
}

void inserir(tipoLista *l, tipoDados x)
{
  tipoNo *aux = (tipoNo *)malloc(sizeof(tipoNo));
  if(!aux) exit(1);
  aux->dado = x;
  aux->prox = l->prim;
  l->prim = aux;
}

void remover(tipoLista *l)
{
  tipoNo *aux;
  if(l)
  {
    aux = l->prim;
    l->prim = l->prim->prox;
    free(aux);
  }
}

void removerN(tipoLista *l, int n)
{
  int i = 0;
  tipoNo *aux = l->prim, *aux2;
  if(aux)
  {
    if(n == 1)
    {
      l->prim = aux->prox; free(aux);
    }else
    {
      aux2 = aux; aux = aux->prox;
        while(aux)
        {
          i++;
          if(i == n)
          {
            aux2->prox = aux->prox; free(aux);
            return;
          }
        }
        aux2 = aux; aux = aux->prox;
    }
  }
}

int main()
{
tipoLista l1;
tipoDados d;
criar(&l1);
d.id = 0;
strcpy(d.nome, "satanas");
strcpy(d.endereco, "grrr");
inserir(&l1,d);
  d.id = 1;
  strcpy(d.nome, "fer");
  strcpy(d.endereco, "grrr");
  inserir(&l1,d);
  d.id = 2;
  strcpy(d.nome, "marcos");
  strcpy(d.endereco, "zl");
  inserir(&l1,d);
  d.id = 3;
  strcpy(d.nome, "nab");
  strcpy(d.endereco, "casa do caralho");
  inserir(&l1,d);
  mostrar(l1);
  removerN(&l1,2);
  mostrar(l1);
  return 0;
}
