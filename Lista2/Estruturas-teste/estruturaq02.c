#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct
{
	int id;
	char nome[20];
	char endereco[40];
}tipoDados;

typedef struct tipoNo
{
  tipoDados dado;
  struct tipoNo *prox;
}tipoNo;

typedef struct tipoLista
{
    tipoNo *prim;
    tipoNo *ult;
}tipoLista;

void criar(tipoLista *l)
{
  l->prim = NULL;
  l->ult = NULL;
}

void inserir(tipoLista *l,tipoDados d)
{
	tipoNo *aux = (tipoNo *)malloc(sizeof(tipoNo));
	if(!aux) exit(1);
	aux->dado = d;
	aux->prox = NULL;
	if(!(l->prim)) l->prim = aux;
	else l->ult->prox = aux;
	l->ult = aux;
}

void remover(tipoLista *l)
{
	tipoNo * aux = l->prim;
	tipoDados x;
	if(l->prim)
	{
		l->prim = l->prim->prox;
		x = aux->dado;
		free(aux);
		if(!(l->prim)) l->ult = NULL;
	}
}

void mostrar(tipoLista l)
{
	while (l.prim)
  {
    printf("%d\n", l.prim->dado.id);
    l.prim = l.prim->prox;
  }
}

int main()
{
	tipoLista l1;
	tipoDados d;
	criar(&l1);
		d.id = 0;
  	strcpy(d.nome, "fer");
  	strcpy(d.endereco, "grrr");
  	inserir(&l1,d);
  	d.id = 1;
  	strcpy(d.nome, "marcos");
  	strcpy(d.endereco, "zl");
  	inserir(&l1,d);
  	d.id = 2;
  	strcpy(d.nome, "nab");
  	strcpy(d.endereco, "casa do caralho");
  	inserir(&l1,d);
		mostrar(l1);
		printf("AAAAAAAAAAAAAAA\n");
		remover(&l1);
		mostrar(l1);
	return 0;
}
