#include<stdio.h>
#include<stdlib.h>
typedef struct tipoNo
{
  int dado;
  struct tipoNo *prox;
}tipoNo;

typedef struct tipoLista
{
    tipoNo *prim;
}tipoLista;

void criar(tipoLista *l)
{
  l->prim = NULL;
}

void inserir(tipoLista *l, int x)
{
  tipoNo *aux = (tipoNo *)malloc(sizeof(tipoNo));
  if(!aux) exit(1);
  aux->dado = x;
  aux->prox = l->prim;
  l->prim = aux;
}

void mostrar(tipoLista l)
{
  while (l.prim)
  {
    printf("%d\n", l.prim->dado);
    l.prim = l.prim->prox;
  }
}

void remover(tipoLista *l)
{
  tipoNo *aux;
  if(l)
  {
    aux = l->prim;
    l->prim = l->prim->prox;
    free(aux);
  }
}

void destruir(tipoLista *l)
{
  tipoNo *aux;
  while(l->prim)
  {
    aux = l->prim;
    l->prim = l->prim->prox;
    l->prim = NULL;
    free(aux);
  }
}

int q14(tipoLista l)
{
  int cont = 0;
  while (l.prim)
  {
    cont += l.prim->dado;
    l.prim = l.prim->prox;
  }
  return cont;
}

int q15(tipoLista l)
{
  if(l.prim)
  {
    while(l.prim->prox)
    {
      if(l.prim->dado >= l.prim->prox->dado) return 0;
      l.prim = l.prim->prox;
    }
  }
  return 1;
}
int main()
{
  tipoLista l1;
  criar(&l1);

  inserir(&l1,6);
  inserir(&l1,6);
  inserir(&l1,5);
  inserir(&l1,4);
  inserir(&l1,3);
  inserir(&l1,2);
  inserir(&l1,1);
  printf("soma:%d\n",q14(l1));
  printf("crescente:%d\n",q15(l1));
  mostrar(l1);

  //q6(&l1,4);
  printf("APAGADO !!!!\n");
  mostrar(l1);

  printf("DESTRUIDA\n");
  destruir(&l1);
  mostrar(l1);
  return 0;
}
