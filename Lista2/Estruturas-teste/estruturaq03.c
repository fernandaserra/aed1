#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct
{
	int id;
	char nome[20];
	char endereco[40];
}tipoDados;

typedef struct tipoNo
{
  tipoDados dado;
  struct tipoNo *prox;
}tipoNo;

typedef struct tipoLista
{
    tipoNo *prim;
}tipoLista;

void criar(tipoLista *l)
{
  l->prim = NULL;
}

void atualizar(tipoLista *l)
{
  l->prim = l->prim->prox;
}

void inserir(tipoLista *l, tipoDados x)
{
  tipoNo *aux = (tipoNo *)malloc(sizeof(tipoNo));
  if(!aux) exit(1);
  aux->dado = x;
  if(!(l->prim))
  {
    l->prim = aux;
    aux->prox = aux;
  }else
  {
    aux->prox = l->prim->prox;
    l->prim->prox = aux;
    l->prim = aux;
  }
}

void remover(tipoLista *l, int chave)
{
  tipoNo *aux, *aux2;
  tipoDados *x = (tipoDados *)malloc(sizeof(tipoDados));
  if(l->prim)
  {
    aux2 = l->prim;
    while ((l->prim->prox) != aux2)
    {
      aux = l->prim->prox;
      l->prim->prox = aux->prox;
      *x = aux->dado; free(aux);
      l->prim = aux2;
    }
    l->prim = l->prim->prox;
  }
  if((aux2->dado.id) == chave)
  {
    l->prim->prox = aux2->prox; l->prim = aux2->prox;
    *x = aux2->dado; free(aux2);
  }
}

/*
void mostrar(tipoLista l)
{
    tipoNo *aux;
    if(l.prim)
    {
      aux = l.prim;
      do (l.prim != aux)
      {
        printf("%d\n", l.prim->dado.id);
        l.prim = l.prim->prox;
      }while(l.prim != aux);
    }
}*/

int contaChave(tipoLista l, int chave)
{
  tipoNo *aux; int cont = 0;
  if(!(l.prim)) return 0;
  aux = l.prim;
    do
    {
      if(l.prim->dado.id == chave) cont++;
      l.prim = l.prim->prox;
    } while(l.prim != aux);
  return cont;
}

int main()
{
  tipoLista l1;
  tipoDados d;
  criar(&l1);

  d.id = 0;
  strcpy(d.nome, "fer");
  strcpy(d.endereco, "grrr");
  inserir(&l1,d);
  d.id = 1;
  strcpy(d.nome, "marcos");
  strcpy(d.endereco, "zl");
  inserir(&l1,d);
  d.id = 2;
  strcpy(d.nome, "nab");
  strcpy(d.endereco, "casa do caralho");
  inserir(&l1,d);
  d.id = 2;
  strcpy(d.nome, "nab");
  strcpy(d.endereco, "casa do caralho");
  inserir(&l1,d);
  d.id = 1;
  strcpy(d.nome, "nab");
  strcpy(d.endereco, "casa do caralho");
  inserir(&l1,d);
  atualizar(&l1);
  //mostrar(l1);

  remover(&l1,2);
  printf("%d\n", contaChave(l1,2));
    return 0;
}
