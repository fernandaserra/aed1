#include <stdio.h>
#include <stdlib.h>

typedef struct
{
  int id;
  float valor;
} tipoDado;

typedef struct tipoNo
{
  tipoDado d;
  struct tipoNo *prox;
} tipoNo;

void inserirElementoLista(tipoNo **prim, tipoDado d)
{
  tipoNo *aux;
  aux = (tipoNo *) malloc (sizeof(tipoNo));
  if(!aux) exit(1); // ALGO DEU MUITO ERRADO!
  aux->d = d;   // ou ainda (*aux).d = d;
  aux->prox = *prim;
  *prim = aux;
}

void mostrarLista(tipoNo *p)
{
  while(p != NULL)
  {   // while(p) {
    printf("id: %d valor: %.2f\n",p->d.id, p->d.valor);
    p = p->prox;
  }
}

int contaOcorrenciasNaLista(tipoNo *p, int chave)
{
  int cont = 0;
  while(p != NULL)
  {   // while(p) {
    if( chave == p->d.id) cont++;
    p = p->prox;
  }
  return cont;
}

tipoDado * buscarNaLista(tipoNo *p, int chave)
{
  while(p != NULL)
  {   // while(p) {
    if( chave == p->d.id)
    {
      return &(p->d);
    }
    p = p->prox;
  }
  return NULL;
}

tipoDado * retornaEnderecoEnesimoNaLista(tipoNo *p, int n) 
{
  int cont =1;
  while(p != NULL)
  {   // while(p) {
    if(cont == n) return &(p->d);
    cont++;
    p = p->prox;
  }
  return NULL;
}

void criarLista(tipoNo **p)
{
  *p = NULL;
}

int main()
{
  tipoNo *l1,*l2;
  tipoDado tmp;
  tipoDado *res;
  int chave;
  int x;

  criarLista(&l1);  // l1 agora está com NULL
  criarLista(&l2);  // l2 agora está com NULL

  tmp.id = 0;
  tmp.valor = 2.0;
  inserirElementoLista(&l1,tmp);
 tmp.id = 1;
  tmp.valor = 3.0;
  inserirElementoLista(&l1,tmp);

  for(x = 0; x < 10 ; x++)
  {
    tmp.id++;
    tmp.valor++;
    inserirElementoLista(&l2,tmp);
  }
  printf("Dados da lista 1:\n");
  mostrarLista(l1);
  printf("Dados da lista 2:\n");
  mostrarLista(l2);

  printf("Entre com um valor a buscar na lista 2:");
  scanf("%d",&chave);
  res = buscarNaLista(l2, chave);
  printf("Buscando %d ...\n",chave);
  if(res)
  {
    printf("***********\n");
    printf("item encontrado. Valor: %.2f\n",res->valor);
    printf("***********\n");

  }
  else
  {
    printf("Item não encontrado\n");
  }

}
