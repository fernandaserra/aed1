#include<stdio.h>
#include<stdlib.h>

typedef struct dado
{
  int x;
} dado;

typedef struct tipoNo
{
  dado d;
  struct tipoNo *prox;
} tipoNo;

/*
  O parâmetro passada para criar uma lista encadeada é um duplo ponteiro
  pois
*/
void criar(tipoNo **p)
{
  *p = NULL;
}

void inserir(tipoNo **p, dado x)
{
  tipoNo *aux;
  aux = (tipoNo *) malloc(sizeof(tipoNo));//Aloca memória para um novo item da lista
  if(!aux) exit(1);
  aux->d = x; //(*aux).d = x (referencia o item dentro de aux)
  aux->prox = *p;
  *p = aux;
}

void mostrar(tipoNo *p)
{
  while(p)
  {
    printf("x = %d\n",p->d.x);
    p = p->prox;
  }
}

int main()
{
  struct tipoNo *l1; //A lista na verdade é apenas o endereço do seu primeiro item
  dado d;
  printf ("sizeof (tipoNo) = %ld\n", sizeof(tipoNo));
  criar(&l1);
  d.x = 0;
  inserir(&l1,d);
  d.x = 1;
  inserir(&l1,d);
  mostrar(l1);
  return 0;
}
