#include<stdio.h>
#include<stdlib.h>

typedef struct
{
  int x;
}tipoDado;

typedef struct tipoNo
{
  tipoDado d;
  struct tipoNo *prox;
}tipoNo;

typedef struct
{
  tipoNo *prim;
}tipoLista;

void criar(tipoLista *l)
{
  l->prim = NULL;
}

void inserir(tipoLista *l, tipoDado dado)
{
  tipoNo *aux = (tipoNo *)malloc(sizeof(tipoNo));
  if(!aux) exit(1);
  aux->d = dado;
  aux->prox = l->prim;
  l->prim = aux;
}

void mostrar(tipoLista l)
{
  while (l.prim)
  {
    printf("%d\n", l.prim->d.x);
    l.prim = l.prim->prox;
  }
}

tipoNo* buscaSequencial(tipoLista l, int index)
{
  int cont = 1;
  while (l.prim)
  {
    if(index == cont) return l.prim;
    else
    {
      l.prim = l.prim->prox;
      cont++;
    }
  }
  return NULL;
}


int buscaBinaria(tipoLista l, int chave, int n)
{
       int inf = 0, sup = n - 1, meio;
       tipoNo *no = (tipoNo *)malloc(sizeof(tipoNo));
       while (inf <= sup)
       {
            meio = (inf + sup)/2;
            no = buscaSequencial(l,meio);
            if (chave == no->d.x) return meio;
            if (chave < no->d.x) sup = (meio - 1);
            else inf = (meio + 1);
       }
       return (-1);
}

int main()
{
  tipoLista l; tipoDado dado; tipoNo *no;
  criar(&l);

  dado.x = 5;
  inserir(&l,dado);
  dado.x = 4;
  inserir(&l,dado);
  dado.x = 3;
  inserir(&l,dado);
  dado.x = 2;
  inserir(&l,dado);
  dado.x = 1;
  inserir(&l,dado);

  mostrar(l);

  no = buscaSequencial(l, 2);
  printf("Valor do index buscado: %d\n", no->d.x);

  printf("Index do elemento: %d\n", buscaBinaria(l,4,5));

  return 0;
}
