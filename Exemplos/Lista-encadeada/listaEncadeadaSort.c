#include<stdio.h>
#include<stdlib.h>

typedef struct
{
  int x;
}tipoDado;

typedef struct tipoNo
{
  tipoDado dado;
  struct tipoNo *prox;
}tipoNo;

typedef struct
{
  tipoNo *prim;
}tipoLista;

void criar(tipoLista *l)
{
  l->prim = NULL;
}

void inserir(tipoLista *l, int d)
{
  tipoNo *aux = (tipoNo *)malloc(sizeof(tipoNo));
  if(!aux) exit(1);
  aux->dado.x = d;
  aux->prox = l->prim;
  l->prim = aux;
}

int size(tipoLista l)
{
  int cont = 0;
  while (l.prim) cont++;
  return cont;
}

void exportToV(tipoLista l, int v[], int n)
{
  int cont = 0;
  while(l.prim)
  {
    if(cont <= n)
    {
      v[cont] = l.prim->dado.x;
      cont++;
    }
    l.prim = l.prim->prox;
  }
}
void exportToL(tipoLista *l, int v[], int n)
{

}
void troca(int *a, int *b)
{
  int aux = *a;
  *a = *b;
  *b = aux;
}

void selectionSort(int v[], int n)
{
  int i, j, min;
  for(int i = 0; i < (n - 1); i++)
  {
    min = i;
    for(j = (i + 1); j < n; j++)
    {
      if(v[j] < v[min]) min = j;
    }
    troca(&v[min],&v[i]);
  }
}

int main()
{
    tipoLista l1;
    int v[5];
    criar(&l1);

    //inserir(&l1,6);
    inserir(&l1,6);
    inserir(&l1,9);
    inserir(&l1,4);
    inserir(&l1,3);
    inserir(&l1,12);
    inserir(&l1,143);

    exportToV(l1,v,5);
    selectionSort(v,5);
    for(int i = 0; i <= 5; i++) printf("%d\n", v[i]);

    //exportVtoL(l1,v,5);

  return 0;
}
