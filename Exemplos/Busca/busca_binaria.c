#include<stdio.h>
#define n 18
int busca(int v[], int chave)
{
  int inf = 0, sup = n - 1, meio;
  while(inf <= sup)
  {
    meio = (inf + sup)/2;
    if(chave == v[meio]) return meio;
    if(chave < v[meio]) sup = meio - 1;
    else inf = meio + 1;
  }
  return -1;
}
int main()
{
  int v[n] = {5,3,9,7,0,3,5,7,9,3,5,7,8,43,2,6,77,87};
  printf("%d\n", busca(v, 9));
  return 0;
}
