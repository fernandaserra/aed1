/*
	Podemos ter ponteiros que apontam para outros ponteiros, recebendo o endereço deles.
	& = endereço
	* = Valor do endereço apontado
*/
#include<stdio.h>
int main()
{
	int x = 10;
	int *v1, **v2, ***v3;
	v1 = &x; //Recebe o endereço da variável x
	v2 = &v1; // Recebe o endereço do ponteiro V1 que contém o endereço da variável x
	v3 = &v2; // Recebe o endereço do ponteiro V2 que contém o endereço do ponteiro V1 [...]
	***v3 = 0; // Zera o valor apontado pelo vetor v3
	printf("%d\n", x);
	return 0;
}
