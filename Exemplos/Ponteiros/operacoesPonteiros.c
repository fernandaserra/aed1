/* O objetivo é fazer com que c = a+ b*/
#include<stdio.h>
int main()
{
  int *p, *q, a, b, c;  // p e q são ponteiros para inteiros
  a = b = 2; c = 0;
  p = &a;  // o valor de p é o endereço de a (p aponta para a)
  q = &b;  // o valor de q é o endereço de b (q aponta para b)
  c = *p + *q; // c é igual a soma das variáveis apontadas por p e q

  return 0;
}
printf("%d\n", c);
