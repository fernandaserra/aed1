/*
	Ponteiros são vetores disfarçados, aceitam o endereço do vetor ao invés dele próprio.
*/
#include<stdio.h>
int main()
{
	int v[50];
	int *p;
	p = v;
	*p = 5;
	p[1] = 3;
	printf("%d\n", v[0]);
	printf("%d\n", v[1]);
	return 0;
}