#include<stdio.h>
#include<stdlib.h>
int main()
{
  int *v, *p;
  v = (int *)malloc(250000000*sizeof(int));
  p = v;
  v[0] = 5;
  p[0] = 4;
  printf("%d\n", v[0]);
  return 0;
}
/*Já que os dois vetores são iguais, possuem o mesmo endereço
de memória, logo mexem na variável que lá reside, não importa
qual dos dois é referenciado pra fazer a mudança do item do vetor.*/
