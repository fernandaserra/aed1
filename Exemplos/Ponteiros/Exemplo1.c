/*
	Passagem por referência, o parâmetro da função é um ponteiro que aponta
para o endereço da varável.
*/

#include<stdio.h>
void zerar(int *x)
{
	*x = 0;
}

int main()
{
	int x = 5;
	zerar(&x);
	printf("%d\n",x);
	return 0;
}

/*
	A passagem por referência é usada quando se deseja alterar o valor da variável dentro de um programa.
*/
