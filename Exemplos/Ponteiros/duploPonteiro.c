/* O objetivo é fazer com que c = a+ b*/
#include<stdio.h>
int main ()
{
  int *p; // p é um ponteiro para inteiro
  int **r; // r é um ponteiro para ponteiro para inteiro (ponteiro que aponta para um ponteiro que aponta para uma variável)
  int  a, b, c;
  a = b = 2; c = 0;
  p = &a;  // p aponta para a
  r = &p;  // r aponta para p e *r aponta para a
  c = **r + b;
  printf("%d\n", c);

  return 0;
}
