#include<stdio.h>
#define n 10
void bubble(int v[])
{
  int aux = 0;
  for(int j = 0; j < n - 1; j++)
  {
    for(int i = 0; i < n-1; i++)
    {
      if(v[i] > v[i+1])
      {
        aux = v[i];
        v[i] = v[i + 1];
        v[i + 1] = aux;
      }
    }
  }
}
int main()
{
  int v[n] = {7,3,9,0,2,1,4,-5,78,45};
  bubble(v);
  for(int i = 0; i < n; i++) printf("%d ", v[i]);
  printf("\n");
  return 0;
}
