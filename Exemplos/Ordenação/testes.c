#include <stdlib.h>
#include <stdio.h>
#define N 100000000


//  inicio 0
// fim = tamanho-1


void ordenaInsercao2(int  v[], int inicio, int fim) 
{
    int *i, *j;
    int *pfim = &v[fim];
    int *pinicio=&v[inicio];
    int pivot;

    
    for(j = &v[inicio+1]; j <= pfim; j++ ) 
    {
      i = j -1;
      pivot = *j;
      while((i>=pinicio) && (*i > pivot)) 
      {
	*(i+1) = *i;
	i--;
      }
      *(i+1) = pivot;
    }
  }




void ordenaInsercao(int  v[], int tamanho) 
{
    int i, j;
    int pivot;

    
    for(j = 1; j < tamanho; j++ ) 
    {
      i = j -1;
      pivot = v[j];
      while((i>=0) && (v[i]>pivot)) 
      {
	  v[i+1] = v[i];
	i--;
      }
      v[i+1] = pivot;
    }
  }




void qSortInterno(int v[], int inicio, int fim);

/* Funcao qickSort a ser chamada em outros pontos do programa. Serve apenas para disparar a chamada revursiva */


void quickSort(int v[], int n) 
{

  qSortInterno(v, 0, n-1);
  ordenaInsercao(v, n);
}

/* Implementacao do quicksort em si. */

void qSortInterno(int v[], int inicio, int fim) 
{
  int pivot;
  int temp;
  int i,j;

  if(fim - inicio > 50) 
  { /* tem mais de 1 elemento */
    i = inicio;
    j = fim;
    pivot = v[(i+j)/2];
    
    do 
    {
      while(v[i] < pivot) i++; /* procura por algum item do lado errado  >= pivot */
      while(v[j] > pivot) j--; /* procura por algum item do lado errado <= pivot */
      if(i<= j) 
      { /* deixa o igual para garantir que ao final i<j */
	temp = v[i];
	v[i] = v[j];
	v[j] = temp;
	i++; j--;
      }
    } while (i<=j);
    qSortInterno(v,inicio, j);
    qSortInterno(v, i,fim);
  }
}






int main() 
{
  int *v;
  int i;
  srand(0);
  //(int*) = casting para remoção de warnings
  v = (int*) malloc (sizeof(int)*N);//alocação dinamica 
  //função que reserva uma área de memória pra ser usada pelo programa
  // a função vai no SO e aloca tantos bytes de memória que vão ser usados pelo programa
  for(i = 0; i< N; i++) 
  {
    v[i]= rand();
  }
  
  printf("iniciando ordenacao...\n");
  quickSort(v, N);
  printf("ordenacao concluida.\n");
  for(i = 0; i < 10; i++) 
  {
    printf("%d ", v[i]);
  }
  printf("\n");
}
//comando 'free' - toda a memória que foi pedida(reservou) é liberada
//parametro : endereço de memória que você alocou
// free(F000)