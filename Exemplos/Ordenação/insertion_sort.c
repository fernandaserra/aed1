#include<stdio.h>
#define n 18
void insertionSort(int v[])
{
   int i, j, pivo;
   for (i = 1; i < n; i++)//começa com i = 1 para que o v[1] seja o pivo
   {
       pivo = v[i];//pivo é dado com v[1]
       j = i - 1;//j recebe i - 1 para que o laço interno percorra todos os itens, inclusive os antes do pivo
       while ((j >= 0) && (v[j] > pivo))//enquanto j não é negativo e o pivo é menor do que os itens percorridos
       {//troca os valores de lugar
           v[j + 1] = v[j];
           j = j - 1;
       }
       v[j + 1] = pivo;
   }
}

int main()
{
  int v[n] = {5,3,9,7,0,3,5,7,9,3,5,7,8,43,2,6,77,87};
  insertionSort(v);
  for(int i = 0; i < n; i++) printf("%d ", v[i]);
  printf("\n");
  return 0;
}
