#include<stdio.h>
#define n 10
void trocar(int v[], int i, int menor)
{
  int aux = v[i];
  v[i] = v[menor];
  v[menor] = aux;
}
void selection(int v[])
{
  for(int i = 0; i < n; i++)
  {
    int menor = i;
    for(int j = (i + 1); j < n; j++)
    {
      if(v[j] < v[menor]) menor = j;
    }
    //trocar(v,i,menor);
    int aux = v[i];
    v[i] = v[menor];
    v[menor] = aux;
  }
}
int main()
{
  int v[n] = {7,3,9,0,2,1,4,-5,78,45};
  selection(v);
  for(int i = 0; i < n; i++) printf("%d ", v[i]);
  printf("\n");
  return 0;
}
