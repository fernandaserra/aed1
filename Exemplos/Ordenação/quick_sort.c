#include<stdio.h>
#define n 18
void ordenaInsercao(int  v[], int tamanho)
{
    int i, j;
    int pivot;
    for(j = 1; j < tamanho; j++ ) {
      i = j -1;
      pivot = v[j];
      while((i >= 0) && (v[i] > pivot))
      {
    	  v[i+1] = v[i];
    	   i--;
      }
      v[i+1] = pivot;
    }
  }
  int main()
  {
    int v[n] = {5,3,9,7,0,3,5,7,9,3,5,7,8,43,2,6,77,87};
    ordenaInsercao(v, n);
    for(int i = 0; i < n; i++) printf("%d ", v[i]);
    printf("\n");
    return 0;
  }
