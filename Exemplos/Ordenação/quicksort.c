
void trocar(int *x, int *y)
{
    int aux = *x;
    *x = *y;
    *y = aux;
}

int particionar(int v[], int inicio, int fim)
{
    int pivot = v[fim], i = (inicio - 1);
    for (int j = inicio; j <= (fim - 1); j++)
    {
      if (v[j] <= pivot)
      {
        i++;
        trocar(&v[i], &v[j]);
      }
    }
    trocar(&v[i + 1], &v[fim]);
    return (i + 1);
}
void quickSort(int v[], int inicio, int fim)
{
    if (inicio < fim)
    {
      int pi = particionar(v, inicio, fim);
      quickSort(v, inicio, (pi - 1));
      quickSort(v, (pi + 1), fim);
    }
}
