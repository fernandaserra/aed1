#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  char nomeCurso[20];
  int id;
  int numAlunos;
} tipoDado;

typedef struct tipoNo {
  tipoDado d;
  struct tipoNo *prox;
} tipoNo;

typedef struct {
  tipoNo *prim,*ult;

} tipoLista;



void inserirElementoInicioLista(tipoLista *l, tipoDado d) {
  tipoNo *aux;

  aux = (tipoNo *) malloc (sizeof(tipoNo));
  if(!aux) {
    exit(1); // ALGO DEU MUITO ERRADO!
  }
  aux->d = d;   // ou ainda (*aux).d = d;
  if(l->prim == NULL) {
    l->ult = aux;
  }
  aux->prox = l->prim;
  l->prim = aux;

}

/*
CONSIDERO QUE O ARQUIVO ESTÁ ABERTO
E QUE CURSOR ESTÁ NA POSICAO QUE QUERO LER */

void carregaListaDoArquivo(tipoLista *l, FILE *fp) {
  tipoDado d;

  while(fread(&d,sizeof(tipoDado),1,fp)) {
    inserirElementoInicioLista(l,d);

  }
}


void inserirElementoFimLista(tipoLista *l, tipoDado d) {
  tipoNo *aux;

  aux = (tipoNo *) malloc (sizeof(tipoNo));
  if(!aux) {
    exit(1); // ALGO DEU MUITO ERRADO!
  }
  aux->d = d;   // ou ainda (*aux).d = d;
  if(l->prim == NULL) {
    l->prim = aux;
  }
  else {
    l->ult->prox = aux;
  }
  l->ult = aux;
  aux->prox = NULL;
}

/* Alguém passa um arquivo já aberto, em modo de escrita e considero que o arquivo já está na posicao para gravacao
 */

void gravaListaEmDisco(tipoLista l, FILE *fp) {
  while(l.prim != NULL) {   // while(p) {
    fwrite(&(l.prim->d),sizeof(tipoDado),1,fp);
    l.prim = l.prim->prox;
}
}



void mostrarLista(tipoLista l) {

  while(l.prim != NULL) {   // while(p) {
    printf("nomeCurso: %s id: %d número de Alunos: %d\n",l.prim->d.nomeCurso,l.prim->d.id, l.prim->d.numAlunos);
    l.prim = l.prim->prox;
}
}


int contaOcorrenciasNaLista(tipoLista l, int chave) {
  int cont = 0;
  while(l.prim != NULL) {   // while(p) {
    if( chave == l.prim->d.id)  {
      cont++;
    }
    l.prim = l.prim->prox;
  }
  return cont;
}


void destroiLista(tipoLista *l) {
  tipoNo *auxPrim,*aux;
  auxPrim = l->prim;
  l->prim = NULL;
  l->ult = NULL;
  while(auxPrim) {
    aux = auxPrim;
    auxPrim = auxPrim->prox;
    free(aux);
  }
}

void removeDaLista(tipoLista *l, char chave[]) {
  tipoNo *ant,*atual;
  ant = l->prim;
  if(ant) {   // Se há elementos na lista
    if( strcmp(chave,ant->d.nomeCurso)== 0)  {//se quero apagar 1o
      l->prim =  ant->prox;
      free(ant);
    } else { // se nao era o 1o, busca no resto da lista
      while(ant->prox != NULL) {   // while(ant->prox) {
	if( strcmp(chave,ant->prox->d.nomeCurso)== 0)  {
	  // entra aqui se achou alguem pra apagar
	  atual = ant->prox;
	  if(atual->prox == NULL) {  l->ult = ant;  }
	  ant->prox = atual->prox;
	  free(atual);
	  return;
	}
	ant = ant->prox;
      }
    }
  }
}





tipoDado * buscarNaLista(tipoLista l, char chave[]) {

  while(l.prim != NULL) {   // while(p) {
    if( strcmp(chave,l.prim->d.nomeCurso)== 0)  {
      return &(l.prim->d);
    }
    l.prim = l.prim->prox;
  }
  return NULL;
}




tipoDado * retornaEnderecoEnesimoNaLista(tipoNo *p, int n) {
  int cont =1;
  while(p != NULL) {   // while(p) {
    if(cont == n){
      return &(p->d);
    }
    cont++;
    p = p->prox;
  }
  return NULL;
}



void criarLista(tipoLista *p) {
  p->prim = NULL;
  p->ult = NULL;
}



void lerString(char stringVar[], int tamanho) {


  fgets(stringVar,tamanho,stdin);
  if(stringVar[strlen(stringVar)-1] == '\n') {
    stringVar[strlen(stringVar)-1] = 0;
  }

}


int main() {
  tipoLista l1;
  tipoDado tmp;
  tipoDado *res;
  char chave[20];
  char continua;
  int x;
  FILE *fp;

  criarLista(&l1);  // l1 agora está com NULL

  // TENTA ABRIR O ARQUIVO PARA LEITURA
  fp = fopen("./arquivoListas","r");
  if(fp != NULL) {
    printf("Carregando dados para a memória\n");
    carregaListaDoArquivo(&l1,fp);
    // Fecha o arquivo
    fclose(fp);
  }

  // INSERINDO NOVOS ELEMENTOS NA LISTA
  do {
    printf("Entre com o id do curso:");
    scanf("%d%*c",&tmp.id);
    printf("Entre com o nome do curso:");
    lerString(tmp.nomeCurso,20);
    printf("Entre com o número de alunos:");
    scanf("%d%*c",&tmp.numAlunos);
     inserirElementoInicioLista(&l1,tmp);
    printf("tecle s para cadastrar mais alguém: ");
    scanf("%c%*c",&continua);
  } while(continua== 's');

  printf("Dados da lista 1:\n");
  mostrarLista(l1);

  printf("Entre com um nome de curso a ser buscado::");
  lerString(chave,20);
  res = buscarNaLista(l1, chave);
  printf("Buscando %s ...\n",chave);
  if(res) {
    printf("***********\n");
    printf("Nome do Curso: %s\n",res->nomeCurso);
    printf("Id           : %d\nNúmero de Alunos: %d\n",res->id,res->numAlunos);
    printf("***********\n");

  }
  else {
    printf("Item não encontrado\n");
  }

  // ZERA O ARQUIVO ANTES DE GRAVAR NOVAMENTE A LISTA
  fp = fopen("./arquivoListas","w");
  if(fp == NULL) {
    printf("Não consegui criar também :(\n");
    exit(1);
  }
  gravaListaEmDisco(l1,fp);
}
