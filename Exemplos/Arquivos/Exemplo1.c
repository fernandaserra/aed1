#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
  char buffer[4000];
  FILE *fp,*fpOut;
  int x=0;
  int lidos;

  if(argc != 3)
  {
    printf("Uso: %s <arq origem> <arq destino>\n",argv[0]);
    exit(1);
  }

  printf("Nome do programa: %s\n",argv[0]);

  fp = fopen(argv[1],"r");
  fpOut = fopen(argv[2],"w");
  do
  {
    lidos = fread(buffer,1,4000,fp);
    fwrite(buffer,1,lidos,fpOut);
  } while(lidos);
}
