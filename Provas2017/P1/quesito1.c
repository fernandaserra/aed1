#include<stdio.h>
#define n 3
int contaLinhas(int m[][n])
{
	int cont = 0;
	for(int i = 0; i < n; i++)
	{
		if((m[i][0] % 2) == 0) cont++;
		printf("%d\n", m[i][0]);
	}
	return cont;
}

int main()
{
	int m[n][n] = {{1,2,3},{4,5,6},{7,8,9}};
	printf("%d\n", contaLinhas(m));
	return 0;
}