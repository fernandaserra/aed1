#include<stdio.h>
#define n 5
void decrescente(int v[])
{
	int n1 = (int) n/2, j = 0, aux = 0;
	for(int i = 0; i < n1; i++)
	{
		j = n - i - 1;
		aux = v[i];
		v[i] = v[j];
		v[j] = aux;
	}
}
int main ()
{
	int v[n] = {1,2,3,4,5};
	decrescente(v);
	for(int i = 0; i < n; i++) printf("%d ", v[i]);
	return 0;
}