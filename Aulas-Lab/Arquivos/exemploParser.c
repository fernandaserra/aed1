#include <stdio.h>
#include <string.h>
#include <stdlib.h>
 #include <locale.h>
typedef struct
{
  char descricao[50];
  float preco;
  int quantidade;
  char cate[5];
} tipoProduto;

int main()
{
  tipoProduto *vet;
  FILE *f, *fout;
  char linha[1000];
  char *tok;
  int cont = 0;
  int x;
  int tam;

 f = fopen("listadeProdutos","r");
 fout = fopen("novoarq","w");

 fscanf(f,"%d\n",&tam);
 printf("Numero de itens: %d\n",tam);
 vet = malloc(sizeof(tipoProduto)*tam);

  setlocale(LC_ALL,"");

  while(fgets(linha,1000,f))
  {
      tok = strtok(linha,":");
      strcpy(vet[cont].descricao,tok);
      tok = strtok(NULL,":");
      vet[cont].preco = atof(tok);
      tok = strtok(NULL, ":");
      vet[cont].quantidade = atoi(tok);
      tok = strtok(NULL, ":");
      while(*tok == ' ') tok++;
      strcpy(vet[cont].cate,tok);
      cont++;
  }
  fwrite(vet,sizeof(tipoProduto),cont,fout);
  printf("Agora vamos ver como ficou o vetor:\n");
  for(x=0; x< cont; x++) {
     printf("Produto: %s\n",vet[x].descricao);
     printf("Preço: %.2f\n",vet[x].preco);
     printf("Quantidade: %d\n",vet[x].quantidade);
     printf("Categoria: %s\n", vet[x].cate);
     printf("**************\n");
  }

}
