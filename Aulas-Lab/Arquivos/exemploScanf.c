#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>

// Assume que vamos ler um arquivo com dados de produtos em formato texto, contendo nome do produto, preco e tipo
// formato do arquivo contém linhas como abaixo:
// <nome_produto_sem_espacos> <preco> <tipo>


typedef struct {
  char produto[20];
  float preco;
  int tipo;
} tipoDado;


int tamanhoArquivo(FILE *f) {

struct stat buf;
fstat(fileno(f), &buf);
return buf.st_size;
}

int leVetorDeProdutos(FILE *fp, tipoDado *v) {
  int cont = 0;
  // para saber quantas linhas tem o arquivo
  fseek(fp,0,SEEK_SET);
  while(fscanf(fp,"%s%f%d",v[cont].produto,&v[cont].preco,&v[cont].tipo) ==3) {
    cont++;
  }
  return cont;
}

/*
fscanf retorna EOF (uma constante) se tentar ler e der erro antes de tentar converter dados durante leitura
retorna o número de dados lidos e convertidos em caso contrário
comparamos com 3 aqui porque cada linha vai ter 3 itens!
*/

int contaLinhas(FILE *fp) {
  int cont = 0;
  tipoDado x;
  // para saber quantas linhas tem o arquivo
  fseek(fp,0,SEEK_SET);
  while(fscanf(fp,"%s%f%d",x.produto,&x.preco,&x.tipo) ==3) {
    cont++;
  }
  return cont;
}

/* mostra dados do vetor */

void mostraDadosVetor(tipoDado *v, int n) {
  int x;

  for(x = 0; x < n; x++) {
    printf("************************\n");
    printf("Posicao %d\n",x);
    printf("Nome do Produto %s\n",v[x].produto);
    printf("Preco do produto %.2f\n",v[x].preco);
    printf("Tipo do produto %d\n",v[x].tipo);

  }

}


int main() {
  FILE *fp;
  tipoDado *vet;
  int numLinhas;

  fp = fopen("testeDados","r+");
  if(fp == NULL) {
    printf("Erro ao abrir arquivo\n");
    exit(1);
  }
  numLinhas = contaLinhas(fp);
  vet = (tipoDado*) malloc(sizeof(tipoDado)*numLinhas);
  printf("Lendo produtos\n");
  leVetorDeProdutos(fp,vet);
  printf("Mostrando Dados\n");
  mostraDadosVetor(vet,numLinhas);

  fclose(fp);
}
