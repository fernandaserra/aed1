#include <stdio.h>
typedef struct tipoPessoa 
{
  char nome[25];
  char sexo; 
  int idade;
} tipoPessoa;

void lerValores(tipoPessoa v[], int n) 
{
  int x;

  for(x = 0; x< n; x++) 
  {
    printf("Dados do [%d]:\n",x);
    printf("nome:");
    scanf("%s%*c",v[x].nome);
    printf("sexo:");
    scanf("%c%*c",&v[x].sexo);
    printf("idade:");
    scanf("%d%*c",&v[x].idade);
  }

}

void mostrarValores(tipoPessoa v[], int n) 
{
  int x;

  for(x = 0; x< n; x++) 
  {
    printf("Dados do [%d]:\n",x);
    printf("nome: %s\n",v[x].nome);
    printf("sexo: %c\n",v[x].sexo);
    printf("idade:%d\n",v[x].idade);
  }

}

int main() 
{
    
  tipoPessoa vet[5];
  lerValores(vet,5);
  mostrarValores(vet,5);
}