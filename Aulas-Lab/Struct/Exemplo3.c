#include <stdio.h>
#include <string.h>
#define TAM_VET 80000

typedef struct tipoProduto 
{
  unsigned codigo;
  float preco;
} tipoProduto;

void preencheVetor(tipoProduto vet[], int tam) 
{
  int x;

  vet[0].codigo = 0;
  vet[0].preco = 100.0;
  for (x = 1 ; x< tam; x++) 
  {
    vet[x].codigo= vet[x-1].codigo+3;  
    vet[x].preco = vet[x-1].preco+5.0; 
  }
}


int buscaSequencial(tipoProduto vet[], int tam, unsigned chave) 
{
  int x;
  
 for (x = 0 ; x< tam; x++) 
 {
   if(vet[x].codigo == chave) return 1;
 }
 return 0;
}


int buscaBinaria(tipoProduto vet[], int tam, unsigned chave) 
{
  int inicio,fim,meio;

  inicio = 0;
  fim = tam - 1;

  while(inicio <= fim ) 
  {
    meio = (inicio+fim)/2;
    if(chave  > vet[meio].codigo) 
    {
      inicio = meio+1;
    }
    else if(chave  < vet[meio].codigo) 
    { 
      fim = meio - 1;
    }
    else 
    {
      return 1;
    }
  }
  return 0;
}



void mostraVetor(tipoProduto vet[], int tam)
{
  int x;

  for (x = 0 ; x< tam; x++) 
  {
    printf("[%d]= (%u; %.2f)\n",x, vet[x].codigo,vet[x].preco);
  }
}



float somaPrecos(tipoProduto vet[], int tam) 
{
  int x;
  float soma=0.0;
  for (x = 0 ; x< tam; x++) 
  {
    if((vet[x].codigo >=3000) && (vet[x].codigo <=4000)) soma = soma + vet[x].preco;
  }
  return soma;
}

int main() 
{
  tipoProduto vet[TAM_VET];
  unsigned valor;
  int x;
  preencheVetor(vet,TAM_VET);
  // mostraVetor(vet,TAM_VET);

  printf("Entre com um codigo:");
  scanf("%u%*c",&valor);
  for(x = 0; x < 1000; x++) 
  {
    if(buscaSequencial(vet,TAM_VET,valor)) printf("Valor encontrado no vetor\n");
  }
}
