#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct dado
{
  int id;
  char nome[256];
} dado;

typedef struct tipoNo
{
  dado d;
  struct tipoNo *prox;
} tipoNo;

void criar(tipoNo **p)
{
  *p = NULL;
}

void inserir(tipoNo **p, dado x)
{
  tipoNo *aux;
  aux = (tipoNo *) malloc(sizeof(tipoNo));//Aloca memória para um novo item da lista
  if(!aux) exit(1);
  aux->d = x; //(*aux).d = x (referencia o item dentro de aux)
  aux->prox = *p;
  *p = aux;
}

void mostrar(tipoNo *p)
{
  while(p)
  {
    printf("id = %d\nnome = %s\n",p->d.id, p->d.nome);
    p = p->prox;
  }
}

char* buscar(tipoNo *p, int chave)
{
  while(p)
  {
    if(p->d.id == chave) return (p->d.nome);
    else p = p->prox;
  }
  return NULL;
}

int contaOcorrencia(tipoNo *p, int chave)
{
  int cont = 0;
  while (p)
  {
    if(chave == (p->d.id)) cont ++;
    p = p->prox;
  }
  return cont;
}

int main()
{
  struct tipoNo *l1; //A lista na verdade é apenas o endereço do seu primeiro item
  dado d;
  char busca[256];
  int resp = 1;
  printf ("sizeof (tipoNo) = %ld\n", sizeof(tipoNo));
  criar(&l1);
  while (resp)
  {
    scanf("%d", &d.id);
    if(d.id == (-1)) resp = 0;
    else
    {
      scanf("%s", d.nome);
      inserir(&l1,d);
    }
  }
  printf("BUSCA:\nDigite o ID a ser buscado:\n");
  scanf("%d",&d.id);
  strcpy(busca,buscar(l1,d.id));
  printf("%s\n", busca);
  printf("%d\n", contaOcorrencia(l1, 2));
  return 0;
}
