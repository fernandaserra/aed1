#include<stdio.h>
#define n 10
int buscaBinaria(int v[], int chave)
{
  int inicio = 0, fim = (n - 1), meio;
  for(int i = 0; i <= n; i++)
  {
    meio = (inicio + fim)/2;
    if(chave == v[meio]) return meio;
    else if(chave > v[meio]) inicio = (meio +1);
    else fim = (meio - 1);
  }
  return (-1);
}

int main()
{
  int v[n] = {1,2,3,4,5,6,7,8,9,10};
  printf("%d\n", buscaBinaria(v,10));
}
