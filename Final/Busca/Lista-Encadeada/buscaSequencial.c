#include<stdio.h>
#include<stdlib.h>
typedef struct
{
  int x;
}tipoDado;

typedef struct tipoNo
{
  tipoDado dado;
  struct tipoNo *prox;
}tipoNo;

typedef struct
{
  tipoNo *prim;
}tipoLista;

void criar(tipoLista *l)
{
  l->prim = NULL;
}

void inserir(tipoLista *l, tipoDado d)
{
  tipoNo *aux = (tipoNo *)malloc(sizeof(tipoNo));
  if(!aux) exit(1);
  aux->dado = d;
  aux->prox = l->prim;
  l->prim = aux;
}

void mostrar(tipoLista l)
{
  while(l.prim)
  {
    printf("%d\n", l.prim->dado.x);
    l.prim = l.prim->prox;
  }
}

int size(tipoLista l)
{
  int cont = 0;
  while (l.prim)
  {
    cont++; l.prim = l.prim->prox;
  }
  return cont;
}

int buscaSequencial(tipoLista l, int chave)
{
  int cont = 0;
  while (l.prim)
  {
    cont ++;
    if(chave == l.prim->dado.x) return cont;
    else l.prim = l.prim->prox;
  }
  return (-1);
}

int main()
{
  tipoLista l; tipoDado dado;
  criar(&l);
  dado.x = 9;
  inserir(&l,dado);
  dado.x = 15;
  inserir(&l,dado);
  dado.x = 2;
  inserir(&l,dado);
  dado.x = 76;
  inserir(&l,dado);
  dado.x = 1;
  inserir(&l,dado);
  mostrar(l);

  printf("%d\n", size(l));

  printf("INDICE:%d\n", buscaSequencial(l,2));
  return 0;
}
