#define n 10
int buscabinaria(int v[], int K)
{
	int inf = 0, sup = K - 1, meio;
	while(inf <= sup)
	{
		meio = (inf+sup)/2;
		if(v[meio] == K) return meio;
		else if (K < v[meio]) sup = meio - 1;
		else inf = meio + 1;
	}
	return -1;
}
