int estaOrdenado(int v[], int N)
{
	int result = 0;
	for(int i = 0; i < N; i++)
	{
		if(v[i] <= v[i + 1]) result = 1;
		else result = 0;
	}
	return result;
}
