int maior(int m[][3], int n)
{
	int maior = m[0][0];
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
			if(maior < m[i][j]) maior = m[i][j];
	}
	return maior;
}