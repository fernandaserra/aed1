int mediana(int v[], int N)
{
  int menor, maior, ig;
  menor = maior = ig = 0;
  for (int i = 0; i < N; i++)
  {
    for (int j = 0; j < N; j++)
    {
      if (v[i] > v[j])  maior++;
      else if (v[i] < v[j])  menor++;
      else if (i != j)  ig++;
    }
    if ((maior == menor) || ((maior + ig) == menor) || ((menor + ig) == maior))  return v[i];
    else maior = menor = ig = 0;
  }
}
