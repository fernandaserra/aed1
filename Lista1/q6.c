void insertion(int v[], int N)
{
   int i = 1, j, chave;
   while(i < N)
   {
       chave = v[i]; j = i - 1;
       while((j >= 0) && (v[j] > chave))
       {
           v[j + 1] = v[j];
           j = j - 1;
       }
       v[j + 1] = chave;
       i++;
   }
}
