double desvio(int v[], int N)
{
  double m, d; m = d = 0;//media e dispersão
  for (int i = 0; i < N; i++)  m += v[i];
  m /= N;
  for (int i = 0; i < N; i++)  d += pow((v[i] - m), 2);
  d /= N;
  return sqrt(d);
}
