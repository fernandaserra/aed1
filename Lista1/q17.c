#define N 6
int acmDiagonal(int v[][N])
{
  int acm = 0;
  for (int l = 0; l < N; l++)
    for (int c = 0; c < N; c++)
    {
      if ((l + c)+ 1 == N) acm += v[l][c];
    }
  return acm;
}
