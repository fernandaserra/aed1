#include<stdio.h>
#include<stdlib.h>
#include<string.h>


typedef struct
{
  char nome[20];
}tipoPessoa;

void swap(tipoPessoa* a, tipoPessoa* b)
{
    tipoPessoa t = *a;
    *a = *b;
    *b = t;
}

int partition (tipoPessoa arr[], int low, int high)
{
    tipoPessoa pivot = arr[high];    // pivot
    int i = (low - 1);  // Index of smaller element

    for (int j = low; j <= high- 1; j++)
    {
        // If current element is smaller than or
        // equal to pivot
        if (strcmp(arr[j].nome,pivot.nome) <= 0)
        {
            i++;    // increment index of smaller element
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i + 1], &arr[high]);
    return (i + 1);
}

/* The main function that implements QuickSort
 arr[] --> Array to be sorted,
  low  --> Starting index,
  high  --> Ending index */
void quickSort(tipoPessoa arr[], int low, int high)
{
    if (low < high)
    {
        /* pi is partitioning index, arr[p] is now
           at right place */
        int pi = partition(arr, low, high);

        // Separately sort elements before
        // partition and after partition
        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}
void printArray(tipoPessoa arr[], int size)
{
    int i;
    for (i=0; i < size; i++)
        printf("%s\n ", arr[i].nome);
    //printf("n");
}

int main()
{
  FILE *fd; tipoPessoa dado, v[4], v1[4];
  fd = fopen("./quesito1.txt","r+");
  if(!fd) exit(1);

  strcpy(dado.nome, "Fernanda");
  v[0] = dado;
  strcpy(dado.nome, "Aluisio");
  v[1] = dado;
  strcpy(dado.nome, "Felipe");
  v[2] = dado;
  strcpy(dado.nome, "Julia");
  v[3] = dado;
  fwrite(v, sizeof(tipoPessoa), 4, fd);
  fclose(fd);
  fd = fopen("./quesito1.txt","r+");
  if(!fd) printf("aaaaaaaa\n");
  int n = sizeof(v)/sizeof(tipoPessoa);
  quickSort(v, 0, n-1);
  fwrite(v, sizeof(tipoPessoa), 4, fd);
  //printf("%s\n", v1[3].nome);
  fclose(fd);
  fd = fopen("./quesito1.txt","r+");
  if(!fd) printf("aaaaaaaa\n");
  fread(v1, sizeof(tipoPessoa), 4, fd);
  for(int i = 0; i < 4; i++)printf("%s\n", v1[i].nome);

  return 0;
}
