#include<stdio.h>
#include<stdlib.h>

typedef struct
{
  int x;
}tipoDado;

typedef struct tipoNo
{
  tipoDado dado;
  struct tipoNo *prox;
}tipoNo;

typedef struct
{
  tipoNo *prim;
}tipoLista;

void criar(tipoLista *l)
{
  l->prim = NULL;
}

void inserir(tipoLista *l, tipoDado d)
{
  tipoNo *aux = (tipoNo *)malloc(sizeof(tipoNo));
  if(!aux) exit(1);
  aux->dado = d;
  aux->prox = l->prim;
  l->prim = aux;
}

void mostrar(tipoLista l)
{
  while (l.prim)
  {
    printf("%d\n", l.prim->dado.x);
    l.prim = l.prim->prox;
  }

}

int size(tipoLista l)
{
  int cont = 0;
  while (l.prim)
  {
    cont++;
    l.prim = l.prim->prox;
  }
  return cont;
}

int temIntersecao(tipoLista l, tipoLista l1)
{
  int sizeL = size(l), sizeL1 = size(l1);
  if(sizeL > sizeL1)
  {
    for(int i = 0; i < sizeL; i++)
    {
      for(int j = 0; i < sizeL1; i++)
      {
        if(l.prim->dado.x == l1.prim->dado.x) return 0;
        else l1.prim = l1.prim->prox;
      }
      l.prim = l.prim->prox;
    }
  }else
  {
      for(int i = 0; i < sizeL1; i++)
      {
        for(int j = 0; i < sizeL; i++)
        {
          if(l.prim->dado.x == l1.prim->dado.x) return 0;
          else l.prim = l.prim->prox;
        }
      }
  }
  return 1;
}

int main()
{
  tipoLista l, l1; tipoDado dado;
  criar(&l); criar(&l1);

  dado.x = 1;
  inserir(&l, dado);
  dado.x = 15;
  inserir(&l, dado);
  dado.x = 4;
  inserir(&l, dado);
  dado.x = 90;
  inserir(&l, dado);

  printf("LISTA 1:\n");
  mostrar(l);

  dado.x = 87;
  inserir(&l1, dado);
  dado.x = 7;
  inserir(&l1, dado);
  dado.x = 90;
  inserir(&l1, dado);

  printf("LISTA 2:\n");
  mostrar(l1);

  printf("Tem interseção:%d\n", temIntersecao(l,l1));
  return 0;
}
