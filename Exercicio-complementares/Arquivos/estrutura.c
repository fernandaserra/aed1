#include<stdio.h>
#include<stdlib.h>

typedef struct
{
  int x;
}tipoDado;

/*
  int n =fwrite(&buffer, bytes, count, file);
  fwrite(ponteiroGenerico, sizeof(tipo do ponteiro generico),quantos itens vao ser escritos, arquivos em que vao ser escritos);

  int n = fread(&buffer, bytes, count, file);
  int nItensLidos = fread(ponteiroGenerico, sizeof(tipo do ponteiro generico), numero de itens que deviam ser lidos, arquivo a ser lido);
*/

int main()
{
  /* TESTES INICIAIS
  FILE *fd;
  fd = fopen("./testezin.txt", "w+");
  fprintf(fd,"CARAI Q VONTADE DE MORRE\n");
  fclose(fd);

  FILE *fd1;
  fd1 = fopen("./numero.txt", "r+");
  if(!fd1) exit(1);
  int x, x1, x2;
  fscanf(fd1,"%d %d %d", &x, &x1, &x2);
  int acm = x + x1 + x2;
  fwrite(&acm,sizeof(int), 1, fd1);
  fclose(fd1);*/

  //Operação de escrita e leitura em um vetor
  FILE *fd; int nLidos, v[5] = {5,10,15,20,25};
  fd = fopen("./vetor.txt","wb");
  if(!fd) exit(1);
  nLidos = fwrite(v, sizeof(int), 5, fd);
  if(nLidos != 5) exit(1);
  fclose(fd);

  fd = fopen("./vetor.txt","rb");
  if(!fd) exit(1);
  int v1[5];
  nLidos = fread(v1,sizeof(int), 5, fd);
  if(nLidos != 5) exit(1);
  fclose(fd);
  for(int i = 0; i < 5; i++) printf("%d\n", v1[i]);
/*
  //Operação de escrita e leitura de uma struct em um arquivo.
  FILE *fd; tipoDado d;
  fd = fopen("./struct.txt", "wb");
  if(!fd) exit(1);
  d.x = 0;
  fwrite(&d,sizeof(tipoDado), 1, fd);
  d.x = 1;
  fwrite(&d,sizeof(tipoDado), 1, fd);
  d.x = 2;
  fwrite(&d,sizeof(tipoDado), 1, fd);
  fclose(fd);

  fd = fopen("./numero.txt", "rb");
  if(!fd) exit(1);
  for(int i = 0; i < 3; i++)
  {
    fread(&d, sizeof(tipoDado), 1, fd);
    printf("%d\n", d.x);
  }
  fclose(fd);
*/
  return 0;
}
