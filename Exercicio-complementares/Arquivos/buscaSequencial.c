#include<stdio.h>
#include<stdlib.h>
#include<string.h>
typedef struct
{
  int id;
  char nome[256];
} tipoPessoa;

tipoPessoa buscaSequencial(FILE *fd, char *chave, int n)
{
  tipoPessoa d; int nLidos;
  for(int i = 0; i < n; i++)
  {
    nLidos = fread(&d,sizeof(tipoPessoa), 1, fd);
    if(strcmp(d.nome, chave) == 0) return d;
  }
}

int main()
{
  FILE *fd; tipoPessoa dado; tipoPessoa v[5];
  fd = fopen("./tipoPessoa.txt", "wb");
  if(!fd) exit(1);
  dado.id = 0;
  strcpy(dado.nome, "Fer");
  v[0] = dado;
  dado.id = 1;
  strcpy(dado.nome, "Bruno");
  v[1] = dado;
  dado.id = 2;
  strcpy(dado.nome, "Flavia");
  v[2] = dado;
  dado.id = 3;
  strcpy(dado.nome, "Celia");
  v[3] = dado;
  dado.id = 4;
  strcpy(dado.nome, "Moacir");
  v[4] = dado;
  dado.id = 5;
  strcpy(dado.nome, "Claudia");
  v[5] = dado;

  int nLidos = fwrite(v,sizeof(tipoPessoa), 5, fd);
  if(nLidos != 5) printf("Nem tudo foi lido\n");
  fclose(fd);
  fd = fopen("./tipoPessoa.txt","rb");
  if(!fd) exit(1);
  dado = buscaSequencial(fd,"Bruno", 5);
  fclose(fd);
  printf("%d\n", dado.id);
  return 0;
}
