#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct
{
  float altura;
  char nome[20], endereco[40];
} tipoPessoa;

void inserir(tipoPessoa v[], int n)
{
  FILE *fd;
  fd = fopen("./saida-out.txt", "wb");
  if(!fd) exit(1);
  for(int i = 0; i < n; i++)
  {
    fprintf(fd,"%s\n%s\n%.2f\n\n", v[i].nome, v[i].endereco, v[i].altura);
  }
  fclose(fd);
}

int main()
{
  FILE *fd; tipoPessoa dado, v[5];
  fd = fopen("./saida-out.txt", "wb");
  strcpy(dado.nome, "fer");
  dado.altura = 1.65;
  strcpy(dado.endereco, "manaux");
  v[0] = dado;
  strcpy(dado.nome, "Bruno");
  dado.altura = 1.59;
  strcpy(dado.endereco, "rj");
  v[1] = dado;
  strcpy(dado.nome, "celinha baggins");
  dado.altura = 1.45;
  strcpy(dado.endereco, "the shire");
  v[2] = dado;
  strcpy(dado.nome, "Moacir");
  dado.altura = 1.73;
  strcpy(dado.endereco, "rj");
  v[3] = dado;
  strcpy(dado.nome, "flavenha");
  dado.altura = 1.55;
  strcpy(dado.endereco, "rj");
  v[4] = dado;
  strcpy(dado.nome, "craudia");
  dado.altura = 1.60;
  strcpy(dado.endereco, "manaox");
  v[5] = dado;
  inserir(v,5);
  fclose(fd);

}
