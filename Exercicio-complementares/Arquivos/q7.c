#include<stdio.h>
#include<stdlib.h>

typedef struct
{
  int id;
  float nota;
}tipoAluno;

int conta(FILE *fd, int n)
{
  tipoAluno d; int cont = 0;
  while(!(feof(fd)))
  {
    fscanf(fd,"%d %f", &d.id, &d.nota);
    if(d.nota > 7) cont++;
  }
  return cont;
}

int main()
{
  FILE *fd; tipoAluno dado, v[5];
  fd = fopen("./tipoAluno.txt","wb");
  dado.id = 0;
  dado.nota = 1.1;
  v[0] = dado;
  dado.id = 1;
  dado.nota = 2.5;
  v[1] = dado;
  dado.id = 2;
  dado.nota = 7.4;
  v[2] = dado;
  dado.id = 3;
  dado.nota = 9.8;
  v[3] = dado;
  dado.id = 4;
  dado.nota = 5.4;
  v[4] = dado;
  dado.id = 5;
  dado.nota = 4.8;
  v[5] = dado;
  for(int i = 0; i < 5; i++)
  {
    fprintf(fd,"%d %.1f ", v[i].id, v[i].nota);
  }
  fclose(fd);

  fd = fopen("./tipoAluno.txt","rb");
  if(!fd) exit(1);
  printf("%d\n", conta(fd, 5));
  fclose(fd);


  return 0;
}
