#include<stdio.h>
#include<stdlib.h>

int calculaTamanho(FILE *fd)
{
  fseek(fd, 0, SEEK_END);
  return (ftell(fd)/sizeof(int));
}

void contagem(FILE *fd, int n)
{
  int v[n], i = 0;
  while(fread(v, sizeof(int), n, fd))
  {
    if(v[i] > '0' && v[i] < '9') printf("%d pertence ao intervalo.\n", v[i]);
    i++;
  }
}

int main()
{
  FILE *fd; int v[10] = {1,2,3,4,86,43,12,54,33,21};
  fd = fopen("./numeros.txt", "w");
  if(!fd) exit(1);

  int nLidos = fwrite(v,sizeof(int), 10, fd);
  if(nLidos != 10) printf("Nem tudo foi lido\n");
  fclose(fd);

  fd = fopen("./numeros.txt","r");
  if(!fd) exit(1);
  contagem(fd, 10);
  fclose(fd);
  return 0;
}
