#include<stdio.h>
#include<stdlib.h>

void transfDados(FILE *fd, FILE *fd1, int n)
{
  int v = 0;
  for(int i = 0; i < n; i++)
  {
    fscanf(fd,"%d",&v);
    fwrite(&v, sizeof(int), 1, fd1);
  }
}

int main()
{
  FILE *fd, *fd1; int v1[5], v = 0;
  fd = fopen("./numeros.txt","r");
  if(!fd) exit(1);
  fd1 = fopen("./saida-numero.txt", "w");
  if(!fd1) exit(1);

  transfDados(fd,fd1,5);

  fclose(fd);
  fclose(fd1);
  return 0;
}
